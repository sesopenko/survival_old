package ca.seanesopenko.survival.controller;

import ca.seanesopenko.survival.entity.Player;
import ca.seanesopenko.survival.model.Level;
import ca.seanesopenko.survival.model.World;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/17/13
 * Time: 9:41 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class PlayerController {

    enum Keys{
        LEFT, RIGHT, UP, DOWN
    }

    private static final float MOVE_DELAY = 0.15f; //ms before moves when holding the keyboard down

    private World world;
    private Player player;

    private boolean currentlyMoving = false;
    private float moveStartTime = 0f;

    private float stateTime = 0f;

    static Map<Keys, Boolean> keys = new HashMap<PlayerController.Keys, Boolean>();
    static {
        keys.put(Keys.LEFT, false);
        keys.put(Keys.RIGHT, false);
        keys.put(Keys.UP, false);
        keys.put(Keys.DOWN, false);
    };

    public PlayerController(World world){
        this.world = world;
    }


    public void upPressed(){
        keys.get(keys.put(Keys.UP, true));
    }

    public void upReleased(){
        keys.get(keys.put(Keys.UP, false));
        endedMove();
    }

    public void rightPressed(){
        keys.get(keys.put(Keys.RIGHT, true));
    }

    public void rightReleased(){
        keys.get(keys.put(Keys.RIGHT, false));
        endedMove();
    }

    public void downPressed(){
        keys.get(keys.put(Keys.DOWN, true));
    }

    public void downReleased(){
        keys.get(keys.put(Keys.DOWN, false));
        endedMove();
    }

    public void leftPressed(){
        keys.get(keys.put(Keys.LEFT, true));
    }

    public void leftReleased(){
        keys.get(keys.put(Keys.LEFT, false));
        endedMove();
    }

    public void startedMove(){
        currentlyMoving = true;
        moveStartTime = stateTime;
    }

    public void endedMove(){
        currentlyMoving = false;
    }

    public void update(float delta){
        stateTime += delta;
        if(currentlyMoving){
            if(stateTime > moveStartTime + MOVE_DELAY){
                endedMove();
            }
        }
        if(!currentlyMoving){
            //TODO:  take input
            Player player = world.getPlayer();
            if(keys.get(Keys.LEFT)){
                if(!playerCollided(-1, 0)){
                    player.moveLeft();
                    startedMove();
                }
            } else if(keys.get(Keys.RIGHT)){
                if(!playerCollided(1,0)){
                    player.moveRight();
                    startedMove();
                }
            } else if(keys.get(Keys.UP)){
                if(!playerCollided(0,1)){
                    player.moveUp();
                    startedMove();
                }
            } else if(keys.get(Keys.DOWN)){
                if(!playerCollided(0,-1)){
                    player.moveDown();
                    startedMove();
                }
            }
        }
        processInput();
    }

    private boolean playerCollided(int moveX, int moveY){
        Player player = world.getPlayer();
        int pX = player.getPosition().x;
        int pY = player.getPosition().y;
        int newX = pX + moveX;
        int newY = pY + moveY;
        Level level = world.getCurrentLevel();
        //are we out of bounds?
        if(pY > level.getHeight()-1){
            return true;
        } else if(pX > level.getWidth()-1){
            return true;
        }
        //did we walk into a block?
        if(level.get(newX, newY) == null || level.get(newX, newY).passable){
            return false;
        } else {
            return true;
        }
    }

    private void processInput() {
        //TODO:  game logic based on keys that are pressed
        //TODO:  figure out how to get current timestamp

    }
}
