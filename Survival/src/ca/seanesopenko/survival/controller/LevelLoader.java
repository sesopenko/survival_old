package ca.seanesopenko.survival.controller;

import java.util.HashMap;
import java.util.Map;
import ca.seanesopenko.survival.model.Block;
import ca.seanesopenko.survival.model.Level;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import ca.seanesopenko.survival.entity.Mob;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/17/13
 * Time: 9:08 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class LevelLoader {

    private static final String LEVEL_PREFIX = "data/levels/level-";
    private static final int BLOCK = 0x000000; //black
    private static final int EMPTY = 0xffffff;
    private static final int START_POS = 0x0000ff;
    private static final int NEXT_POS = 0x00ff00;

    private static final int MOB_BYSTANDER = 0xbb00ff;
    private static final int MOB_SWAT = 0xff0000;
    private static final int MOB_OFFICER = 0xc8c800;
    private static final int MOB_SECURITY = 0x919191;

    static Map<String, Block> blockMap = new HashMap<String, Block>();
    static{
        Block block = new Block();
        block.textureName = "block";
        block.passable = false;
        blockMap.put("block", block);
    }


    public static Level loadLevel(int number){
        Level level = new Level();

        //loading png into a pixmap
        Pixmap pixmap = new Pixmap(Gdx.files.internal(LEVEL_PREFIX + number + ".png"));

        //setting the size of the level based on the size of the pixmap

        level.setWidth(pixmap.getWidth());
        level.setHeight(pixmap.getHeight());

        //creating the backing blocks array
        Block[][] blocks = new Block[level.getWidth()][level.getHeight()];
        for(int col = 0; col < level.getWidth(); col++){
            for (int row = 0; row < level.getHeight(); row++){
                blocks[col][row] = null;
            }
        }

        for(int row = 0; row < level.getHeight(); row++){
            for(int col = 0; col < level.getWidth(); col++){
                int pixel = (pixmap.getPixel(col, row) >>> 8) & 0xffffff;
                int iRow = level.getHeight() - 1 - row;
                /*
                    image needs to be flipped on the y axis because it's reading from top to bottom and
                    filling from bottom to top
                 */
                int x = col;
                int y = level.getHeight() - 1 - row;
                switch(pixel){
                    case BLOCK:
                        blocks[x][y] = blockMap.get("block");
                        break;
                    case START_POS:
                        level.setStartPos(x, y);
                        break;
                    case NEXT_POS:
                        level.setNextPos(x, y);
                        break;
                    case MOB_BYSTANDER:
                        level.addMob(new Mob(x, y, Mob.Type.BYSTANDER));
                        break;
                    case MOB_SECURITY:
                        level.addMob(new Mob(x, y, Mob.Type.SECURITY));
                        break;
                    case MOB_OFFICER:
                        level.addMob(new Mob(x, y, Mob.Type.OFFICER));
                        break;
                    case MOB_SWAT:
                        level.addMob(new Mob(x, y, Mob.Type.SWAT));
                        break;
                    default:
                        //empty space
                        break;
                }
            }
        }
        level.setBlocks(blocks);
        return level;
    }
}
