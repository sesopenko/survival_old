package ca.seanesopenko.survival.entity;

import ca.seanesopenko.survival.math.Integer2;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/20/13
 * Time: 5:20 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class Mob  extends Entity {

    public enum Type{
        BYSTANDER,
        SECURITY,
        OFFICER,
        SWAT,
    }

    private Type type;
    private int hp = 100;

    public Mob(Integer2 position, Type type){
        super(position);
        this.type = type;
    }

    public Mob(int x, int y, Type type){
        super(x, y);
        this.type = type;
    }

    public int getPoints(){
        switch(type){
            case BYSTANDER:
                return 800;
            case SECURITY:
                return 200;
            case OFFICER:
                return 400;
            case SWAT:
            default:
                return 800;
        }
    }

    public Type getType(){
        return type;
    }

    public void damage(int damage, boolean isExplosive){
        if(isExplosive){
            hp -= damage;
            return;
        }
        switch(type){
            case OFFICER:
                //light body armor
                applyArmorDamage(0.5f, damage);
                break;
            case SWAT:
                //heavy body armor
                applyArmorDamage(0.2f, damage);
                break;
            case SECURITY:
            case BYSTANDER:
            default:
                //no body armor
                applyArmorDamage(0.8f, damage);
                break;
        }
    }

    /**
     * This is meant for bullet damage with some mobs wearing armor
     * @param armor possible reduction in armor
     * @param damage
     */
    private void applyArmorDamage(float armor, int damage){
        int max = damage;
        int min = Math.round(armor * (float)damage);
        //generate a random number between the min & max
        int actual = min + (int)(Math.random() * ((max - min) + 1));
        hp -= damage;
    }

    public boolean isDead(){
        if(hp <= 0){
            return true;
        } else {
            return false;
        }
    }



}
