package ca.seanesopenko.survival.entity;
/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/16/13
 * Time: 7:29 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */

import ca.seanesopenko.survival.entity.Entity;
import ca.seanesopenko.survival.math.Integer2;
import com.badlogic.gdx.math.Vector2;

public class Player extends Entity{
    public Player(Integer2 position){
        super(position);
    }

    public Player(){
        super();
    }

    public Player(Vector2 position){
        super(position);
    }
}
