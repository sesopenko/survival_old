package ca.seanesopenko.survival.entity;

import ca.seanesopenko.survival.math.Integer2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/16/13
 * Time: 7:27 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class Entity {

    public static final float SIZE = 48f;

    protected Integer2 position;
    protected Double animTime;
    protected Rectangle bounds;
    protected Direction direction;

    public enum Direction{
        LEFT, RIGHT, UP, DOWN
    }

    public Entity(){
        this.bounds = new Rectangle(0f, 0f, SIZE, SIZE);
        setPosition(new Integer2());
        setDirection(Direction.DOWN);
    }

    public Entity(int x, int y){
        this.bounds = new Rectangle(0f, 0f, SIZE, SIZE);
        setPosition(x,y);
        setDirection(Direction.DOWN);
    }

    public Entity(Integer2 position){
        this.bounds = new Rectangle(0f, 0f, SIZE, SIZE);
        setPosition(position);
        setDirection(Direction.DOWN);
    }

    public Entity(Vector2 position){
        this.bounds = new Rectangle(0f, 0f, SIZE, SIZE);
        setPosition(position);
        setDirection(Direction.DOWN);
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public Direction getDirection(){
        return this.direction;
    }

    public Integer2 getPosition(){
        return this.position;
    }

    public void setPosition(int x, int y){
        Integer2 newPos = new Integer2(x, y);
        setPosition(newPos);

    }

    public void setPosition(Integer2 position){
        this.position = position;
        this.bounds.x = (float)position.x;
        this.bounds.y = (float)position.y;
    }

    public void setPosition(Vector2 position){
        setPosition(new Integer2((int)position.x, (int)position.y));
    }

    public void moveRight(){
        setPosition(position.x + 1, position.y);
        this.setDirection(Direction.RIGHT);
    }

    public void moveLeft(){
        setPosition(position.x - 1, position.y);
        this.setDirection(Direction.LEFT);
    }

    public void moveUp(){
        setPosition(position.x, position.y + 1);
        this.setDirection(Direction.UP);
    }

    public void moveDown(){
        setPosition(position.x, position.y - 1);
        this.setDirection(Direction.DOWN);
    }


    public Vector2 getFloatPosition(){
        return new Vector2((float)position.x, (float)position.y);
    }

    public void setAnimTime(Double animTime){
        this.animTime = animTime;
    }

    public Double getAnimTime(){
        return this.animTime;
    }

}
