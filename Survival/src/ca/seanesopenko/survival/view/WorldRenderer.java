package ca.seanesopenko.survival.view;

import ca.seanesopenko.survival.entity.Entity;
import ca.seanesopenko.survival.entity.Mob;
import ca.seanesopenko.survival.entity.Player;
import ca.seanesopenko.survival.model.Block;
import ca.seanesopenko.survival.model.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/16/13
 * Time: 8:13 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class WorldRenderer {

    private static final float CAMERA_WIDTH = 10f;
    private static final float CAMERA_HEIGHT = 7f;
    private static final float RUNNING_FRAME_DURATION = 0.06f; //not correct speed right now

    private World world;

    private OrthographicCamera cam;

    ShapeRenderer debugRenderer = new ShapeRenderer();

    private Map<Entity.Direction, TextureRegion> playerMap;
    private Map<Entity.Direction, TextureRegion> bystanderMap;
    private Map<Entity.Direction, TextureRegion> securityMap;
    private Map<Entity.Direction, TextureRegion> officerMap;
    private Map<Entity.Direction, TextureRegion> swatMap;

    private SpriteBatch spriteBatch;

    private float ppuX;
    private float ppuY;

    private int width;
    private int height;

    static Map<String, TextureRegion> blockMap;

    public WorldRenderer(World world){
        playerMap = new HashMap<Entity.Direction, TextureRegion>();
        bystanderMap = new HashMap<Entity.Direction, TextureRegion>();
        securityMap = new HashMap<Entity.Direction, TextureRegion>();
        officerMap = new HashMap<Entity.Direction, TextureRegion>();
        swatMap = new HashMap<Entity.Direction, TextureRegion>();
        this.blockMap = new HashMap<String, TextureRegion>();
        this.world = world;
        this.cam = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
        spriteBatch = new SpriteBatch();
        loadTextures();
    }

    public void setSize(int w, int h){
        this.width = w;
        this.height = h;
        ppuX = (float)width / CAMERA_WIDTH;
        ppuY = (float)height / CAMERA_HEIGHT;

    }

    public void render(){
        cam.position.set((float) world.getPlayer().getPosition().x, (float) world.getPlayer().getPosition().y, 0f);
        cam.update();
        spriteBatch.begin();
        spriteBatch.setProjectionMatrix(cam.combined);
        drawBlocks();
        drawMobs();
        drawPlayer();
        spriteBatch.end();
    }

    private void drawBlocks(){
        //TODO:  write draw blocks
        Block[][] blocks = world.getCurrentLevel().getBlocks();
        for(int row = 0; row <  world.getCurrentLevel().getHeight();row++){
            for(int col = 0; col < world.getCurrentLevel().getWidth(); col++){
                if(blocks[row][col] != null){
                    TextureRegion texture = blockMap.get(blocks[row][col].textureName);
                    spriteBatch.draw(texture, (float)row , (float)col, Block.SIZE, Block.SIZE);
                }
            }
        }
    }

    private void drawMobs(){
        Array<Mob> mobs = world.getCurrentLevel().getMobs();
        for(Mob mob : mobs){
            switch(mob.getType()){
                case BYSTANDER:
                    TextureRegion texture = bystanderMap.get(mob.getDirection());
                    spriteBatch.draw(texture, (float)mob.getPosition().x, (float)mob.getPosition().y, Block.SIZE, Block.SIZE);
                    break;
                case SECURITY:
                    //TODO:  render security
                    break;
                case OFFICER:
                    //TODO:  render officer
                    break;
                case SWAT:
                    //TODO:  render swat
                    break;
                default:
                    //DO NOTHING
                    break;
            }
        }
    }

    private void drawPlayer(){
        Player player = world.getPlayer();
        TextureRegion playerTexture = playerMap.get(player.getDirection());
        spriteBatch.draw(playerTexture, player.getFloatPosition().x, player.getFloatPosition().y, Block.SIZE, Block.SIZE);
    }

    private void loadTextures(){
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/entities/entities.txt"));

        loadEntityTextures(atlas, playerMap, "player");
        loadEntityTextures(atlas, bystanderMap, "bystander");
        loadEntityTextures(atlas, securityMap, "security");
        loadEntityTextures(atlas, officerMap, "officer");
        loadEntityTextures(atlas, swatMap, "swat");

        TextureRegion blockTexture = atlas.findRegion("block");
        blockTexture.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        blockMap.put("block", blockTexture);

    }

    private void loadEntityTextures(TextureAtlas atlas, Map entityMap, String entityName){
        TextureRegion idleRight = atlas.findRegion(entityName+"-right");
        TextureRegion idleLeft = new TextureRegion(idleRight);
        idleLeft.flip(true, false);
        TextureRegion idleUp = atlas.findRegion(entityName + "-up");
        TextureRegion idleDown = new TextureRegion(idleUp);
        idleDown.flip(false, true);
        entityMap.put(Entity.Direction.UP, idleUp);
        entityMap.put(Entity.Direction.RIGHT, idleRight);
        entityMap.put(Entity.Direction.LEFT, idleLeft);
        entityMap.put(Entity.Direction.DOWN, idleDown);
    }

}
