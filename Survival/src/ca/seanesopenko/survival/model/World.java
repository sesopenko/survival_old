package ca.seanesopenko.survival.model;

import ca.seanesopenko.survival.controller.LevelLoader;
import ca.seanesopenko.survival.entity.Player;
import ca.seanesopenko.survival.math.Integer2;
import com.badlogic.gdx.Gdx;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/16/13
 * Time: 8:14 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class World {
    private Player player;
    private Level currentLevel;
    public World(){
        this.player = new Player(new Integer2(0,0));
        this.currentLevel = LevelLoader.loadLevel(1);
        this.player.setPosition(this.currentLevel.getStartPos());
        Gdx.app.log("LoadedPlayer", this.player.getFloatPosition().x + "," + this.player.getFloatPosition().y);
    }

    public Player getPlayer(){
        return this.player;
    }

    public Level getCurrentLevel(){
        return this.currentLevel;
    }
}
