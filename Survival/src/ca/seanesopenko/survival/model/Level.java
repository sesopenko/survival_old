package ca.seanesopenko.survival.model;

import ca.seanesopenko.survival.entity.Mob;
import ca.seanesopenko.survival.math.Integer2;
import com.badlogic.gdx.utils.Array;


/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/17/13
 * Time: 9:10 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class Level {
    private int width;
    private int height;
    private Block[][] blocks;

    private int startX;
    private int startY;
    private int nextX;
    private int nextY;

    private Array<Mob> mobs;

    public Level(){
        startX = 0;
        startY = 0;
        nextX = 0;
        nextY = 0;
        width = 10;
        height = 10;
        blocks = new Block[width][height];
        for(int row = 0; row < height; row++){
            for(int col = 0; col < width; col++){
                blocks[col][row] = null;
            }
        }
        mobs = new Array<Mob>();

    }

    public Level(int width, int height, Block[][] blocks){
        this.width = width;
        this.height = height;
        this.blocks = blocks;
        this.startX = 0;
        this.startY = 0;
        this.nextX = 0;
        this.nextY = 0;
    }

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    public void setWidth(int width){
        this.width = width;
    }

    public void setHeight(int height){
        this.height = height;
    }

    public void setStartPos(int x, int y){
        this.startX = x;
        this.startY = y;
    }

    public void setNextPos(int x, int y){
        this.nextX = x;
        this.nextY = y;
    }

    public Integer2 getStartPos(){
        return new Integer2(startX, startY);
    }

    public Block[][] getBlocks(){
        return blocks;
    }

    public void setBlocks(Block[][] blocks){
        this.blocks = blocks;
    }

    public Block get(int x, int y){
        return blocks[x][y];
    }

    public void addMob(Mob mob){
        mobs.add(mob);
    }

    public Array<Mob> getMobs(){
        return mobs;
    }

}
