package ca.seanesopenko.survival.model;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/17/13
 * Time: 9:13 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class Block {
    public static final float SIZE = 1f;
    public String textureName = "block";
    public boolean passable = false;
}
