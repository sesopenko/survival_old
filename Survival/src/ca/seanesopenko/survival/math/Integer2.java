package ca.seanesopenko.survival.math;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/16/13
 * Time: 7:33 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */
public class Integer2 {
    public int x,y;

    public Integer2(){
        this.x = this.y = 0;
    }

    public Integer2(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void Add(Integer2 other){
        this.x += other.x;
        this.y += other.y;
    }

    public void Subtract(Integer2 other){
        this.x -= other.x;
        this.y -= other.y;
    }

    public void Multiply(Integer2 other){
        this.x *= other.x;
        this.y *= other.y;
    }

    public void Divide(Integer2 other){
        this.x /= other.x;
        this.y /= other.y;
    }

    public Integer2 Modulus(int modulus){
        return new Integer2(this.x % modulus, this.y % modulus);
    }
}
