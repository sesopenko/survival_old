package ca.seanesopenko.survival.screen;

/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 4/16/13
 * Time: 7:17 PM
 * (c) Sean Esopenko 2013 all rights reserved
 * www.seanesopenko.ca
 * sesopenko@gmail.com
 */

import ca.seanesopenko.survival.controller.PlayerController;
import ca.seanesopenko.survival.model.World;
import ca.seanesopenko.survival.view.WorldRenderer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.Input.Keys;

public class GameScreen implements Screen, InputProcessor {
    private World world;
    private WorldRenderer renderer;
    private PlayerController playerController;

    private int width;
    private int height;

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        playerController.update(delta);
        renderer.render();

    }

    @Override
    public void resize(int width, int height) {
        renderer.setSize(width, height);
        this.width = width;
        this.height = height;
    }

    @Override
    public void show() {
        world = new World();
        renderer = new WorldRenderer(world);
        playerController = new PlayerController(world);
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void pause() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void resume() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void dispose() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public boolean keyDown(int keycode) {
        switch(keycode){
            case Keys.LEFT:
                playerController.leftPressed();
                break;
            case Keys.RIGHT:
                playerController.rightPressed();
                break;
            case Keys.UP:
                playerController.upPressed();
                break;
            case Keys.DOWN:
                playerController.downPressed();
                break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode){
            case Keys.LEFT:
                playerController.leftReleased();
                break;
            case Keys.RIGHT:
                playerController.rightReleased();
                break;
            case Keys.UP:
                playerController.upReleased();
                break;
            case Keys.DOWN:
                playerController.downReleased();
                break;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean scrolled(int amount) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
